pragma solidity ^0.8.4;

import "./token.sol";

contract FileHelper is Token {
    mapping (address => Cloud) internal addressToCloud;

    event FileCreated(string name);
    event FileUpdated(string name);

    struct Cloud {
        mapping (uint => File) files;
        uint filesSize;
    }

    struct File {
        string name;
        bytes datas;
        bool isPublic;
    }

    /*
    * Effectue des vérifications multiples selon l'action
    */
    modifier fileExist(string memory _name, string memory _action) {
        string[] memory _filesName = showFiles();
        if (keccak256(abi.encode(_action)) == keccak256(abi.encode("addData")) || keccak256(abi.encode(_action)) == keccak256(abi.encode("setFileIsPublic"))) {
            bool _fileExist = false;
            for (uint i = 0; i < _filesName.length; i++) {
                if (keccak256(abi.encode(_name)) == keccak256(abi.encode(_filesName[i]))) {
                    _fileExist = true;
                }
            }
            require (_fileExist == true, "file does not exist");
        } else if (keccak256(abi.encode(_action)) == keccak256(abi.encode("newFile"))) {
            for (uint i = 0; i < _filesName.length; i++) {
                require (keccak256(abi.encode(_name)) != keccak256(abi.encode(_filesName[i])), "file already exist");
            }
        }
        _;
    }

    /**
    * @dev Crée un nouveau fichier.
    */
    function newFile(string memory _name, string memory _datas, bool _isPublic) public fileExist(_name, "newFile") {
        Cloud storage _myCloud = addressToCloud[msg.sender];
        _myCloud.files[_myCloud.filesSize] = File(_name, abi.encode(_datas), _isPublic);
        emit FileCreated(_name);
        _myCloud.filesSize++;
    }

    /**
    * @return Retourne les données d'un fichier.
    */
    function readFile(string memory _name) view public returns(string memory) {
        Cloud storage _myCloud = addressToCloud[msg.sender];
        for (uint i = 0; i < _myCloud.filesSize; i++) {
            if (keccak256(abi.encode(_myCloud.files[i].name)) == keccak256(abi.encode(_name))) {
                return abi.decode(_myCloud.files[i].datas, (string));
            }
        }
        return "file not found";
    }

    /**
    * @return Retourne la liste des fichiers de l'adresse.
    */
    function showFiles() public view returns(string[] memory) {
        Cloud storage _myCloud = addressToCloud[msg.sender];
        string[] memory _files = new string[](_myCloud.filesSize);
        for (uint i = 0; i < _myCloud.filesSize; i++) {
            _files[i] = _myCloud.files[i].name;
        }
        return _files;
    }

    /**
    * Ajoute des données à un fichier.
    */
    function addData(string memory _fileName, string memory _datas) public fileExist(_fileName, "addData") {
        Cloud storage _myCloud = addressToCloud[msg.sender];
        string[] memory _filesName = showFiles();
        for (uint i = 0; i < _filesName.length; i++) {
            if (keccak256(abi.encode(_fileName)) == keccak256(abi.encode(_filesName[i]))) {
                _myCloud.files[i].datas = abi.encode(bytes.concat(abi.decode(_myCloud.files[i].datas, (bytes)), bytes(_datas)));
            }
        }
        emit FileUpdated(_fileName);
    }

    /**
    * Modifie la visibilité publique d'un fichier
    */
    function setFileIsPublic(string memory _name, bool _isPublic) public fileExist(_name, "setFileIsPublic") {
        Cloud storage _cloud = addressToCloud[msg.sender];
        string[] memory _filesName = showFiles();
        for (uint i = 0; i < _filesName.length; i++) {
            if (keccak256(abi.encode(_name)) == keccak256(abi.encode(_filesName[i]))) {
                _cloud.files[i].isPublic = _isPublic;
            }
        }
        emit FileUpdated(_name);
    }
}