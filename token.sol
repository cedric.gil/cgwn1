pragma solidity ^0.8.4;

import "./ownable.sol";

contract Token is Ownable {
    uint public totalSupply = 10000000;
    string public name = "CGWN1";
    string public symbol = "CGWN1";

    mapping(address => uint) public addressToBalance;

    event Transfer(address indexed from, address indexed to, uint value);

    /**
    * @dev Attribue l'intégralité des tokens à l'adresse qui lance le contrat.
    */
    constructor () {
        addressToBalance[msg.sender] = totalSupply;
    }

    /**
    * @return Retourne la quantité de token que l'adresse possède.
    *
    */
    function balanceOf(address _owner) public view returns(uint) {
        return addressToBalance[_owner];
    }

    /**
    * @dev Transfert la valeur demandée d'une adresse à un autre.
    */
    function transfer(address _to, uint _value) public returns(bool) {
        require (balanceOf(msg.sender) >= _value, "balance too low");
        addressToBalance[_to] += _value;
        addressToBalance[msg.sender] -= _value;
        emit Transfer(msg.sender, _to, _value);
        return true;
    }

    /**
    * Brûle des tokens.
    */
    function burn(uint _value) public onlyOwner {
        require (_value <= balanceOf(msg.sender));
        addressToBalance[msg.sender] -= _value;
        totalSupply -= _value;
        emit Transfer(msg.sender, address(0), _value);
    }
}