# CGWN1
Application décentralisée de stockage d'informations, déployée sur la blockchain BEP-20 (Binance Smart Chain).  
Vous pouvez rendre certaines de vos informations accessible à tous.  
Adresse du contrat : 0xBF55e3132393B3a718e0d4D10B2a8Ed23b320cD1
<br/>
<br/>
<br/>
Le fichier principal est file_public.sol et le nom du contrat est FilePublic
<br/>
<br/>
**Les fonctions de gestion des fichiers :**

Crée un nouveau fichier appartenant à l'adresse qui envoie la transaction.  
Les paramètres correspondent au nom du fichier, à son contenu, et à s'il est accessible publiquement ou non.  
`newFile(string name, string datas, bool isPublic)`

Ajoute du contenu à un fichier existant.  
Les paramètres correspondent au nom du fichier, et au contenu à ajouter.  
`addData(string fileName, string datas)`

Renvoie le contenu d'un fichier.  
Le paramètre correspond au nom du fichier.  
`readFile(string name) returns(string)`

Renvoie la liste des fichiers qui appartient à l'adresse qui envoie la transaction.  
`showFiles() returns(string[])`

Modifie la visibilité publique d'un fichier.  
Les paramètres correspondent au nom du fichier, et à s'il est accessible publiquement ou non.  
`setFileIsPublic(string name, bool isPublic)`

Renvoie le contenu d'un fichier qui appartient à une autre adresse.  
Les paramètre correspondent au nom du fichier, et à l'adresse de son propriétaire.  
`readPublicFile(string name, address address) returns(string)`

Renvoie la liste des fichiers publiques qui appartient à une autre adresse.  
Le paramètre correspond à l'adresse propriétaire des fichiers.   
`showPublicFiles(address address) returns(string[])`
<br/>
<br/>
<br/>
**Les fonctions de gestion des tokens :**

Renvoie la quantité de tokens d'une adresse.  
Le paramètre correspond à l'adresse propriétaire des tokens.  
`balanceOf(address owner) returns(uint)`

Transfert des tokens d'une adresse à une autre.  
Les paramètres correspondent à l'adresse du destinataire, et à la quantité de token à envoyer.  
`transfer(address to, uint value) returns(bool)`
