pragma solidity ^0.8.4;

import "./file_helper.sol";

contract FilePublic is FileHelper {
    /**
    * @return Retourne la liste des fichiers publiques d'une adresse.
    */
    function showPublicFiles(address _address) public view returns(string[] memory) {
        Cloud storage _cloud = addressToCloud[_address];
        string[] memory _filesName = new string[](_cloud.filesSize);
        uint _cpt = 0;
        for (uint i = 0; i < _cloud.filesSize; i++) {
            if (_cloud.files[i].isPublic == true) {
                _filesName[_cpt] = _cloud.files[i].name;
                _cpt++;
            }
        }
        return _filesName;
    }

    /**
    * @return Retourne les données d'un fichier de l'adresse spécifiée.
    */
    function readPublicFile(string memory _name, address _address) public view returns(string memory) {
        Cloud storage _cloud = addressToCloud[_address];
        for (uint i = 0; i < _cloud.filesSize; i++) {
            if (keccak256(abi.encode(_cloud.files[i].name)) == keccak256(abi.encode(_name))) {
                if (_cloud.files[i].isPublic == true) {
                    return abi.decode(_cloud.files[i].datas, (string));
                }
            }
        }
        return "file not found";
    }
}